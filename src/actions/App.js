// action types
export const GREETING = 'GREETING';
export const SEARCH = 'SEARCH';

import data from '../reducers/data.js';

// action creators
export const greeting = (message) => {
  return {
    type: GREETING,
    message: message
  }
};

export const search = (keyword) => {
  let benefits = {};
  Object.keys(data.benefits).map((key) => (
    key === keyword
    ? benefits[key] = data.benefits[key]
    : null
  ))
  return {
    type: SEARCH,
    data: benefits
  }
};
