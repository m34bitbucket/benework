// action types
export const GREETING = 'GREETING';

// action creators
export const greeting = (message) => {
  return {
    type: GREETING,
    message: message
  }
};
