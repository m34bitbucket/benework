import React from 'react';
import {
  Card,
  CardTitle,
  GridList,
  GridTile,
  Paper,
  RaisedButton,
  Subheader,
  TextField
} from 'material-ui';
import { Link } from 'react-router-dom';
import MediaQuery from 'react-responsive';

import Header from './Header.jsx';
import { app } from '../styles.js';

class App extends React.Component {
  constructor(props) {
    super(props);
  }

  componentWillMount() {
    this.setState({
      benefits: this.props.app.benefits
    });
  }

  render() {
    const search = () => {
      let keyword = this.refs.keyword.getValue() || ''
      keyword === '映画' ? keyword='cinema' :null
      keyword === 'ジム' ? keyword='gym' :null
      keyword === 'ワイン' ? keyword='wine' :null
      keyword === '果物' ? keyword='fruits' :null
      keyword === 'レストラン' ? keyword='restaurant' :null
      keyword === 'テーマパーク' ? keyword='themePark' :null
      this.props.search(keyword);
    }

    return(
      <article>
        <Header />

        {/* for desktop or laptop */}
        <MediaQuery query="(min-width: 1224px)">
          <section style={app.banner}>
            <section style={app.onBanner}>
              <h1 style={app.title}>あなたに最適な福利 "幸" 生を見つけましょう</h1>
              <Paper style={app.paper} zDepth={1}>
                <TextField ref="keyword" floatingLabelText="検索キーワード" fullWidth={true} hintText="e.g.) ハワイ旅行" style={app.textField} />
                <RaisedButton label="さがす" primary={true} onClick={search} />
              </Paper>
            </section>
          </section>

          <section style={app.benefits}>
            <GridList cellHeight={205} cols={3}>
              <Subheader style={app.benefits.title}>人気の福利幸生</Subheader>
              {
                Object.keys(this.props.app.benefits).map((key) => (
                  <GridTile key={key} style={app.grid.tile} subtitle={this.props.app.benefits[key].abstract} title={this.props.app.benefits[key].title}
                    containerElement={<Link to={{ pathname: '/detail/'+key }} />}
                  >
                    <div style={{
                      background: "url(" + this.props.app.benefits[key].banner + ") center / cover",
                      height: 205
                    }}></div>
                  </GridTile>
                ))
              }
            </GridList>
          </section>
        </MediaQuery>

        {/* for table or mobile */}
        <MediaQuery query="(max-width: 1224px)">
        <section>
          <Subheader>あなたにピッタリの福利 "幸" 生を見つけましょう</Subheader>
          <section style={app.mobile.search}>
            <TextField ref="keyword" floatingLabelText="検索キーワード" fullWidth={true} hintText="e.g.) ハワイ旅行" />
            <RaisedButton label="さがす" primary={true} onClick={search} />
          </section>
        </section>
        <section>
          <Subheader>人気の福利幸生</Subheader>
          {
            Object.keys(this.props.app.benefits).map((key) => (
              <GridTile key={key} style={app.grid.tile} subtitle={this.props.app.benefits[key].abstract} title={this.props.app.benefits[key].title}
                containerElement={<Link to={{ pathname: '/detail/'+key }} />}
              >
                <div style={{
                  background: "url(" + this.props.app.benefits[key].banner + ") center / cover",
                  height: 205
                }}></div>
              </GridTile>
            ))
          }
        </section>
        
        </MediaQuery>

        <section style={app.footer}>
          &copy; 2017 benework production committee.
        </section>
      </article>
    );
  }
}

export default App;
