import React from 'react';
import {
  Avatar,
  Card,
  CardMedia,
  CardTitle,
  CardText,
  FontIcon,
  FlatButton,
  GridList,
  GridTile,
  IconButton,
  List,
  ListItem,
  Paper,
  Subheader,
  StarBorder
} from 'material-ui';
import { Link } from 'react-router-dom';
import { InteractiveForceGraph, ForceGraphNode, ForceGraphLink } from 'react-vis-force';

import Header from './Header.jsx';
import { detail } from '../styles.js';

class Detail extends React.Component {
  constructor(props) {
    super(props);
  }

  componentWillMount() {
    this.setState({
      benefits: this.props.detail.benefits,
      users: this.props.detail.users,
      members: this.props.detail.benefits[this.props.match.params.key].members,
      companies: this.props.detail.companies
    });
  }

  render() {
    return(
      <article>
        <Header leftIcon="chevron_left" />

        <section>
          <Card>
            <CardTitle title={this.state.benefits[this.props.match.params.key].title} subtitle={this.state.benefits[this.props.match.params.key].abstract} />
          </Card>
        </section>

        <section style={detail.root}>
          <GridList
            cols={2}
            cellHeight={200}
            padding={1}
            style={detail.gridList}
          >
            {
              Object.keys(this.state.benefits[this.props.match.params.key].banners).map( (key) => {
                return (
                  <GridTile
                    key={key}
                    titlePosition="top"
                    titleBackground="linear-gradient(to bottom, rgba(0,0,0,0.7) 0%,rgba(0,0,0,0.3) 70%,rgba(0,0,0,0) 100%)"
                    cols={this.state.benefits[this.props.match.params.key].banners[key].featured ? 2 : 1}
                    rows={this.state.benefits[this.props.match.params.key].banners[key].featured ? 2 : 1}
                  >
                    <img src={this.state.benefits[this.props.match.params.key].banners[key].image} width="100%" />
                  </GridTile>
                )
              })
            }
          </GridList>
        </section>

        <section>
          <Card>
            <CardText>
              {this.state.benefits[this.props.match.params.key].detail}
            </CardText>

            <List>
              <Subheader>参加メンバー</Subheader>
              {
                Object.keys(this.state.benefits[this.props.match.params.key].members).map( (key) => {
                  return(
                    <ListItem
                      leftAvatar={
                        <Avatar src={this.state.users[this.state.benefits[this.props.match.params.key].members[key]].image} />
                      }
                      containerElement={<Link to={{pathname:"/user/"+this.state.benefits[this.props.match.params.key].members[key]}} />}
                    >
                      {this.state.users[this.state.benefits[this.props.match.params.key].members[key]].name}
                    </ListItem>
                  )
                })
              }
            </List>
          </Card>
        </section>

        <section>
          <Card>
            <Subheader>Network</Subheader>
            <InteractiveForceGraph
              simulationOptions={{
                height: 300,
                width: 300,
                animate: true,
                showLabels: true
              }}
              labelAttr="label"
              highlightDependencies
            >
              {
                this.state.members.map((member) => {
                  let options = {
                    node: {
                      id: member,
                      label: this.state.users[member].name,
                      radius: this.state.users[member].relation.length * 2
                    },
                    fill: this.state.companies[this.state.users[member].company].color
                  };
                  return (
                    <ForceGraphNode node={options.node} fill={options.fill} stroke="#ffffff" strokeWidth={0.5} />
                  )
                })
              }
              {
                this.state.members.map((member) => {
                  let jsx = [];
                  this.state.users[member].relation.map((target) => {
                    if (this.state.members.includes(target)) {
                      let link = {
                        source: member,
                        target: target,
                        value: 2
                      };
                      jsx.push(<ForceGraphLink link={link} stroke="#000000" />)
                    }
                  })
                  return jsx
                })
              }
            </InteractiveForceGraph>
          </Card>
        </section>

        <section style={detail.footer}>
          &copy; 2017 benework production committee.
        </section>
      </article>
    );
  }
}

export default Detail;
