import React from 'react';
import { AppBar, FontIcon, IconButton } from 'material-ui';
import { Link, withRouter } from 'react-router-dom';

class Header extends React.Component {
  componentWillMount() {
    this.setState({
      leftIcon: <IconButton><FontIcon className="material-icons" onClick={(event) => {
        this.props.history.push('/')
      }}>{this.props.leftIcon || 'bubble_chart'}</FontIcon></IconButton>,
    });
  }

  render() {
    return (
      <article>
        <AppBar title="benework" iconElementLeft={this.state.leftIcon} />
      </article>
    );
  }
}

export default withRouter(Header);
