import React from 'react';
import {
  AppBar,
  Avatar,
  CardActions,
  Card,
  CardMedia,
  CardTitle,
  CardText,
  CardHeader,
  Chip,
  FlatButton,
  GridList,
  GridTile,
  List,
  ListItem,
  Subheader,
  StarBorder
} from 'material-ui';
import { Link } from 'react-router-dom';
import { user } from '../styles.js';
import Header from './Header.jsx';

class User extends React.Component {
  constructor(props) {
    super(props);
  }

  componentWillMount() {
    this.setState({
      benefits: this.props.user.benefits,
      users: this.props.user.users,
      companies: this.props.user.companies
    });
  }

  render() {
    return(
      <article>
        <Header leftIcon="chevron_left" />

        <section style={user.root}>
          <Card zDepth={0}>
            <CardHeader
              title={this.state.users[this.props.match.params.key].name}
              subtitle={this.state.users[this.props.match.params.key].place + " "+this.state.users[this.props.match.params.key].age + "歳 " + this.state.users[this.props.match.params.key].bloodtype + "型"}
              avatar={this.state.users[this.props.match.params.key].image}
            />
            <Subheader>所属</Subheader>
            <CardText>
              {this.state.companies[this.state.users[this.props.match.params.key].company].name}
            </CardText>
            <Subheader>自己紹介</Subheader>
            <CardText>
              {this.state.users[this.props.match.params.key].profile}
            </CardText>
            <CardText>
              <div style={user.wrapper}>
                <Chip style={user.chip} >{this.state.users[this.props.match.params.key].sports}</Chip>
                <Chip style={user.chip} >{this.state.users[this.props.match.params.key].hobby}</Chip>
                </div>
            </CardText>
            <List>
              <Subheader>友人</Subheader>
              {
                Object.keys(this.state.users[this.props.match.params.key].relation).map( (key) => {
                  return(
                    <ListItem key={key}
                      leftAvatar={
                        <Avatar src={this.state.users[this.state.users[this.props.match.params.key].relation[key]].image} />
                      }
                      containerElement={<Link to={{pathname:"/user/"+this.state.users[this.props.match.params.key].relation[key]}} />}
                    >
                      {this.state.users[this.state.users[this.props.match.params.key].relation[key]].name}
                    </ListItem>
                  )
                })
              }
            </List>
          </Card>
        </section>

        <section style={user.root}>
            <Subheader>利用している福利厚生</Subheader>
            {
              Object.keys(this.state.benefits).map( (key) => {
                return(
                  Object.keys(this.state.benefits[key].members).map( (member) => {
                      return(
                        this.state.benefits[key].members[member] === this.props.match.params.key
                        ? <Card>
                            <CardMedia 
                              overlay={<CardTitle
                              title={this.state.benefits[key].title}
                              subtitle={this.state.benefits[key].abstract} />}
                              containerelement={ <Link to={{ pathname: '/detail/' + key }} /> }
                            >
                              <img src={this.state.benefits[key].banner} alt="" />
                            </CardMedia>
                          </Card>
                        : <div></div>
                      )
                  })
                )
              })
            }
        </section>

        <section style={user.footer}>
          &copy; 2017 benework production committee.
        </section>
      </article>
    );
  }
}

export default User;
