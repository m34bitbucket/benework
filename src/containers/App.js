import React from 'react';
import { connect } from 'react-redux';

import App from '../components/App.jsx';
import * as action from '../actions/App.js';

const mapStateToProps = (state) => {
  return state;
};
  
const mapDispatchToProps = (dispatch) => {
  return {
    greeting: (message) => {
      dispatch(action.greeting(message));
    },
    search: (keyword) => {
      dispatch(action.search(keyword));
    }
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(App);
