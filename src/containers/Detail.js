import React from 'react';
import { connect } from 'react-redux';

import Detail from '../components/Detail.jsx';
import * as action from '../actions/Detail.js';

const mapStateToProps = (state) => {
  return state;
};
  
const mapDispatchToProps = (dispatch) => {
  return {
    greeting: (message) => {
      dispatch(action.greeting(message));
    }
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(Detail);
