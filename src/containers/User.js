import React from 'react';
import { connect } from 'react-redux';

import User from '../components/User.jsx';
import * as action from '../actions/User.js';

const mapStateToProps = (state) => {
  return state;
};
  
const mapDispatchToProps = (dispatch) => {
  return {
    greeting: (message) => {
      dispatch(action.greeting(message));
    }
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(User);
