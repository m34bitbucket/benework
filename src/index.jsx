import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { HashRouter as Router, Redirect, Route, Switch } from 'react-router-dom';
import { getMuiTheme, MuiThemeProvider } from 'material-ui/styles';
import beneworkTheme from 'material-ui/styles/baseThemes/beneworkTheme';

import store from './store.js';
import App from './containers/App.js';
import Detail from './containers/Detail.js';
import User from './containers/User.js';

ReactDOM.render(
  <Provider store={store}>
    <Router>
      <MuiThemeProvider muiTheme={getMuiTheme(beneworkTheme)}>
        <Switch>
          <Route exact path="/" component={App} />
          <Route exact path="/detail/:key" component={Detail} />
          <Route exact path="/user/:key" component={User} />
          <Redirect to="/" />
        </Switch>
      </MuiThemeProvider>
    </Router>
  </Provider>,
  document.querySelector("#root")
);
