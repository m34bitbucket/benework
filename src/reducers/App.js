import { GREETING, SEARCH } from '../actions/App.js';
import data from './data.js';

const initialState = data;

const appReducer = (state = initialState, action) => {
  switch(action.type) {
    case GREETING: {
      return Object.assign({}, state, {
        message: action.message
      });
    }

    case SEARCH: {
      return Object.assign({}, state, {
        benefits: action.data
      });
    }
    
    default: {
      return state;
    }
  }
};

export default appReducer;
