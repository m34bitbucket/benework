import { GREETING } from '../actions/Detail.js';
import data from './data.js';

const initialState = data;

const detailReducer = (state = initialState, action) => {
  switch(action.type) {
    case GREETING: {
      return Object.assign({}, state, {
        message: action.message
      });
    }
    default: {
      return state;
    }
  }
};

export default detailReducer;
