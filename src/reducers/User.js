import { GREETING } from '../actions/User.js';
import data from './data.js';

const initialState = data;

const userReducer = (state = initialState, action) => {
  switch(action.type) {
    case GREETING: {
      return Object.assign({}, state, {
        message: action.message
      });
    }
    default: {
      return state;
    }
  }
};

export default userReducer;
