import {
  amber500,
  blue500,
  cyan500,
  indigo500,
  orange500,
  red500,
  yellow500
} from 'material-ui/styles/colors';

const data = {
  users: {
    user1:{
      image:"http://realsound.jp/wp-content/uploads/2017/09/20170906_tkuc-947x633.jpg",
      name:"山口利一",
      male:"男性",
      place:"東京都豊島区",
      age:"29",
      bloodtype:"A",
      birthplace:"鹿児島県",
      sports:"テニス、フットサル、読書",
      company:"sales1",
      hobby:"食べ歩き、スポーツ",
      profile:"営業一部の食べ歩き担当です！いろんな部署の人と仲良くしてつながりたいのでよろしくです。",
      relation: [
        "user2","user3","user4","user5","user6","user7","user8","user9","user10",
      ],

    },
    user2:{
      image:"http://livedoor.blogimg.jp/garlsvip/imgs/7/9/797af6d2-s.jpg",
      name:"川村明子",
      male:"女性",
      place:"東京都新宿区",
      age:"27",
      bloodtype:"A",
      birthplace:"神奈川県",
      sports:"テニス",
      company:"sales2",
      hobby:"カメラ、旅行",
      profile:"よろしくですー。カメラと旅行が好きなので好きです！",
      relation: [
        "user5","user6","user7","user8","user9","user10",
      ],

    },
    user3:{
      image:"https://rr.img.naver.jp/mig?src=http%3A%2F%2Fimgcc.naver.jp%2Fkaze%2Fmission%2FUSER%2F20170322%2F70%2F7763940%2F0%2F268x362x258df750eb420720d962b744.jpg%2F300%2F600&twidth=300&theight=600&qlt=80&res_format=jpg&op=r",
      name:"大河内康弘",
      male:"男性",
      place:"東京都練馬区",
      age:"27",
      bloodtype:"B",
      birthplace:"群馬県",
      sports:"なし",
      company:"develop",
      hobby:"ゲーム、読書",
      profile:"インドア派でFPSなどe-Sportsスキ",
      relation: [
        "user11","user12","user13","user14","user15","user16","user17","user18","user19","user20",
      ],

    },
    user4:{
      image:"https://rr.img.naver.jp/mig?src=http%3A%2F%2Fpic.prepics-cdn.com%2F7a5b76c8f8cbf%2F40181841.jpeg&twidth=1000&theight=0&qlt=80&res_format=jpg&op=r",
      name:"阿部聡",
      male:"男性",
      place:"東京都練馬区",
      age:"29",
      bloodtype:"B",
      birthplace:"大阪府",
      sports:"野球",
      company:"sales2",
      hobby:"ラーメン屋めぐり",
      profile:"ラーメンめぐりで全国まわりました。おいしいラーメンならなんでもきいてね",
      relation: [
        "user21","user22","user23",
      ],

    },
    user5:{
      image:"https://rr.img.naver.jp/mig?src=http%3A%2F%2Fimgcc.naver.jp%2Fkaze%2Fmission%2FUSER%2F20160722%2F70%2F7763940%2F39%2F800x1089x748175c8b02abc6b332bb13.jpg%2F300%2F600&twidth=300&theight=600&qlt=80&res_format=jpg&op=r",
      name:"高木博",
      male:"男性",
      place:"神奈川県鶴見区",
      age:"27",
      bloodtype:"B",
      birthplace:"大阪府",
      sports:"バスケ",
      company:"marketing",
      hobby:"読書",
      profile:"読書が好きです。小説とか好きですー",
      relation: [
        "user1","user12","user3","user14","user5","user16","user7",
      ],

    },
    user6:{
      image:"http://livedoor.blogimg.jp/chikyuuch/imgs/8/3/831d6969.jpg",
      name:"山木学",
      male:"男性",
      place:"千葉県千葉市",
      age:"23",
      bloodtype:"AB",
      birthplace:"愛知県",
      sports:"野球",
      company:"sales1",
      hobby:"映画",
      profile:"すきあらば映画いってます。月に三回はいきますよ！",
      relation: [
        "user1","user12","user3","user14","user5","user16","user7",
      ],
    },
    user7:{
      image:"http://livedoor.blogimg.jp/garlsvip/imgs/6/4/64d9d757.jpg",
      name:"吉岡理穂",
      male:"女性",
      place:"東京都練馬区",
      age:"22",
      bloodtype:"A",
      birthplace:"神奈川県",
      sports:"ビリヤード",
      company:"sales1",
      hobby:"スポーツ",
      profile:"スポーツ全般好きです",
      relation: [
        "user1","user12","user3","user14","user5","user16","user7",
      ],
    },
    user8:{
      image:"http://livedoor.blogimg.jp/garlsvip/imgs/9/0/9070e2f5-s.jpg",
      name:"本田翼",
      male:"女性",
      place:"東京都練馬区",
      age:"29",
      bloodtype:"A",
      birthplace:"埼玉県",
      sports:"テニス",
      company:"finance",
      hobby:"食べ歩き",
      profile:"食べ歩き大好き。デブ活してます",
      relation: [
        "user1","user12","user3","user14","user5","user16","user7",
      ],
    },
    user9:{
      image:"http://livedoor.blogimg.jp/chikyuuch/imgs/a/d/ad0c6f2e.jpg",
      name:"玉木博",
      male:"男性",
      place:"東京都港区",
      age:"27",
      bloodtype:"AB",
      birthplace:"埼玉県",
      sports:"フットサル",
      company:"corporate",
      hobby:"スポーツ鑑賞",
      profile:"スポーツ鑑賞好きで自分でもします",
      relation: [
        "user1","user12","user3","user14","user5","user16","user7",
      ],
    },
    user10:{
      image:"http://livedoor.blogimg.jp/chikyuuch/imgs/7/9/79cbc207.jpg",
      name:"玉山哲司",
      male:"男性",
      place:"東京都目黒区",
      age:"26",
      bloodtype:"B",
      birthplace:"神奈川県",
      sports:"バレーボール",
      company:"sales3",
      hobby:"音楽鑑賞",
      profile:"音楽鑑賞好きで夏はフェス天国！！",
      relation: [
        "user1","user12","user3","user14","user5","user16","user7",
      ],
    },
    user11:{
      image:"http://livedoor.blogimg.jp/chikyuuch/imgs/f/5/f5946917.jpg",
      name:"山寺浩二",
      male:"男性",
      place:"東京都練馬区",
      age:"25",
      bloodtype:"B",
      birthplace:"福岡県",
      sports:"テニス",
      company:"sales3",
      hobby:"ライブ",
      profile:"昔バンドやってて良くライブやフェスいきます。",
      relation: [
        "user1","user12","user3","user14","user5","user16","user7",
      ],
    },
    user12:{
      image:"http://livedoor.blogimg.jp/chikyuuch/imgs/4/3/438fc6cb.jpg",
      name:"渋谷義春",
      male:"男性",
      place:"神奈川県鶴見区",
      age:"26",
      bloodtype:"A",
      birthplace:"京都府",
      sports:"バスケ",
      company:"corporate",
      hobby:"音楽鑑賞",
      profile:"洋楽ダイスキです。バスケも社外サークルはいってます",
      relation: [
        "user1","user12","user3","user14","user5","user16","user7",
      ],
    },
    user13:{
      image:"http://livedoor.blogimg.jp/chikyuuch/imgs/a/a/aa1a6bff.jpg",
      name:"阿部信三",
      male:"男性",
      place:"千葉県千葉市",
      age:"29",
      bloodtype:"A",
      birthplace:"東京都",
      sports:"バスケ",
      company:"develop",
      hobby:"スポーツ鑑賞",
      profile:"スポーツなんでもみます",
      relation: [
        "user1","user12","user3","user14","user5","user16","user7",
      ],
    },
    user14:{
      image:"http://livedoor.blogimg.jp/chikyuuch/imgs/f/c/fc48547a.png",
      name:"小泉順次郎",
      male:"男性",
      place:"千葉県千葉市",
      age:"32",
      bloodtype:"AB",
      birthplace:"東京都",
      sports:"ランニング",
      company:"develop",
      hobby:"ネットサーフィン",
      profile:"ネットサーフィンかランニングしかしてないです。",
      relation: [
        "user1","user12","user3","user14","user5","user16","user7",
      ],
    },
    user15:{
      image:"http://livedoor.blogimg.jp/garlsvip/imgs/3/d/3dc7ce93-s.jpg",
      name:"田中真紀子",
      male:"女性",
      place:"東京都台東区",
      age:"31",
      bloodtype:"B",
      birthplace:"神奈川県",
      sports:"FPS",
      company:"develop",
      hobby:"FPS",
      profile:"リアルは嫌いなのでオンラインでFPSしましょう",
      relation: [
        "user1","user12","user3","user14","user5","user16","user7",
      ],
    },
    user16:{
      image:"http://livedoor.blogimg.jp/chikyuuch/imgs/b/3/b34483df.jpg",
      name:"山添俊彦",
      male:"男性",
      place:"東京都墨田区",
      age:"27",
      bloodtype:"AB",
      birthplace:"神奈川県",
      sports:"皇居ラン",
      company:"marketing",
      hobby:"テレビゲーム",
      profile:"FPSからRPGまでゲーム好きです！新発売の日は有給（笑）",
      relation: [
        "user1","user12","user3","user14","user5","user16","user7",
      ],
    },
    user17:{
      image:"http://livedoor.blogimg.jp/garlsvip/imgs/9/7/9768c032.jpg",
      name:"新垣結",
      male:"女性",
      place:"東京都新宿区",
      age:"23",
      bloodtype:"A",
      birthplace:"愛知県",
      sports:"スノーボード",
      company:"sales2",
      hobby:"カフェ",
      profile:"カフェめぐりしてますー。鎌倉のトイカフェがお気に入り",
      relation: [
        "user1","user12","user3","user14","user5","user16","user7",
      ],
    },
    user18:{
      image:"http://livedoor.blogimg.jp/garlsvip/imgs/c/d/cd669f31-s.jpg",
      name:"山田愛",
      male:"女性",
      place:"東京都世田谷区",
      age:"22",
      bloodtype:"B",
      birthplace:"北海道",
      sports:"スノーボード",
      company:"marketing",
      hobby:"カメラ",
      profile:"一年目です！新しいもの色々吸収したいです！よろしくです",
      relation: [
        "user1","user12","user3","user14","user5","user16","user7",
      ],
    },
    user19:{
      image:"http://livedoor.blogimg.jp/chikyuuch/imgs/7/c/7c57426c.jpg",
      name:"本仮屋幸弘",
      male:"男性",
      place:"東京都練馬区",
      age:"24",
      bloodtype:"A",
      birthplace:"沖縄県",
      sports:"スキー",
      company:"develop",
      hobby:"プログラム",
      profile:"三年目で仕事もなれてきたので横のつながりつくりたいですー。ハッカソン仲間募集！",
      relation: [
        "user1","user12","user3","user14","user5","user16","user7",
      ],
    },
    user20:{
      image:"http://livedoor.blogimg.jp/garlsvip/imgs/4/2/4240171d-s.jpg",
      name:"北村明子",
      male:"女性",
      place:"東京都渋谷区",
      age:"25",
      bloodtype:"AB",
      birthplace:"神奈川県",
      sports:"水泳",
      company:"sales2",
      hobby:"旅行",
      profile:"旅行が好きで休みがあれば南国まで泳ぎに行きます！ダイビングもよくします",
      relation: [
        "user1","user12","user3","user14","user5","user16","user7",
      ],
    },
    user21:{
      image:"http://livedoor.blogimg.jp/garlsvip/imgs/1/3/13a11498-s.jpg",
      name:"吉田愛梨",
      male:"女性",
      place:"東京都中央区",
      age:"26",
      bloodtype:"A",
      birthplace:"栃木県",
      sports:"水泳",
      company:"sales1",
      hobby:"街歩き",
      profile:"週末は一人ぶらり旅してます。一緒に食べ歩き街歩きしましょ",
      relation: [
        "user1","user12","user3","user14","user5","user16","user7",
      ],
    },
    user22:{
      image:"http://livedoor.blogimg.jp/garlsvip/imgs/d/1/d1f56a78-s.jpg",
      name:"北村咲",
      male:"女性",
      place:"東京都渋谷区",
      age:"28",
      bloodtype:"A",
      birthplace:"青森県",
      sports:"ランニング",
      company:"corporate",
      hobby:"皇居ラン",
      profile:"皇居ランよくやってるので興味ある人はぜひー",
      relation: [
        "user1","user12","user3","user14","user5","user16","user7",
      ],
    },
    user23:{
      image:"http://livedoor.blogimg.jp/garlsvip/imgs/9/5/956255e1-s.jpg",
      name:"山村順子",
      male:"女性",
      place:"東京都中央区",
      age:"28",
      bloodtype:"AB",
      birthplace:"三重県",
      sports:"スノーボード",
      company:"marketing",
      hobby:"焼肉",
      profile:"焼肉大好きです",
      relation: [
        "user1","user12","user3","user14","user5","user16","user7",
      ],
    },
  },
  benefits: {
    "cinema": {
      title: "最新映画が毎月1本無料",
      abstract: "映画好きのあなたにオススメ！TOKYO シネマズで放映される最新映画の無料クーポンチケットが毎月届きます。",
      banner:"/assets/img/cinema-min.jpg",
      banners: {
        "banner0":{
          image:"/assets/img/cinema-min.jpg",
          featured: true,
        },
        "banner1":{
          image:"https://www.alpinehearingprotection.com/wp-content/uploads/2015/10/earplugs-cinema-hearing-protection_small.jpg",
        },
        "banner2":{
          image:"http://www.pescarawebtv.it/img/41df11c7648072043e62d66525ecc6bb/w/864/h/400/thumb/3724.jpg",
        },
      },
      detail: "benefitマンデイ お得な特典！ 特典1 会員特典 映画鑑賞料金1,100円 特典2 ポップコーンセットが半額！ 320円（税込）割引対象劇場 TOHOシネマズ全劇場（お台場シネマメディアージュも含む）",
      members: [ "user1","user2","user3","user4","user5","user6","user7","user8","user9","user10" ],
    },
    "fruits": {
      title: "果物セット定期便",
      abstract: "四季の果物を3ヶ月ごとにお届けします。届け先を選択できるので御中元・御歳暮にも最適！",
      banner:"/assets/img/fruits-min.jpg",
      banners: {
        "banner0":{
          image:"/assets/img/fruits-min.jpg",
          featured: true,
        },
        "banner1":{
          image:"https://grapee.jp/wp-content/uploads/19945_main.jpg",
        },
        "banner2":{
          image:"https://cdn.macaro-ni.jp/image/summary_body/14/14484/72f2e6c46e4e93b636e382b3b5e9f1d6.jpg",
        },
      },
      detail: "お得な定期便 おとなのプチ贅沢セット 4,980円/月（税別）一流産地のフルーツを、一流のベテランが目利きする。だから大田市場のフルーツは美味しいのです。 そのような最高のフルーツを、お手頃価格でお届けできないか？ そういった思いから、国産フルーツ定期宅配「果物の達人」はスタートしました。 果物の達人が選び抜いたフルーツを毎月みなさまのご自宅までお届けします。",
      members: [ "user11","user12","user13","user14","user15" ],
    },
    "gym": {
      title: "EveryGYM 最大20％割引",
      abstract: "EveryGYM 月額利用料がどなたでも15％オフになります。さらに新規会員登録頂いた方は20％オフ！",
      banner:"/assets/img/gym-min.jpg",
      banners: {
        "banner0":{
          image:"/assets/img/gym-min.jpg",
          featured: true,
        },
        "banner1":{
          image:"http://prnavi.jp/wp-content/uploads/2013/05/ar0010009407o.jpg",
        },
        "banner2":{
          image:"https://kintorecamp-huvjjtmj02bn.netdna-ssl.com/wp-content/uploads/2016/12/dreamstime_s_31409229-e1481606775433.jpg",
        },
      },
      detail: "初心者からアスリートまで、全ての人々が結果を出せるようあらゆることについて考え抜かれたフィットネスクラブです。50年以上の歴史とトレーニングにおける圧倒的な実績を誇り、世界30カ国、700店以上、300万人を越える世界最大級のフィットネスクラブ（スポーツクラブ・スポーツジム）です。",
      members: [ "user1","user22","user23","user14","user15","user16","user7", ],
    },
    "restaurant": {
      title: "【肥ってから】ペアディナー招待券【痩せろ】",
      abstract: "提携レストランでご利用頂ける招待券を隔月でお届けします。ペアでコース料理を注文されますと、1名様分が無料になります。",
      banner:"/assets/img/restaurant-min.jpg",
      banners: {
        "banner0":{
          image:"/assets/img/restaurant-min.jpg",
          featured: true,
        },
        "banner1":{
          image:"http://renaissance-okinawa.com/wordpress/wp-content/uploads/2014/03/dinner.jpg",
        },
        "banner2":{
          image:"http://uds.gnst.jp/rest/img/4ps0wvr80000/s_00in.jpg?t=1428286023",
        },
      },
      detail: "「友人や仲間を誘って、気軽に行けるレストランはどこ？」そんな時におすすめしたい一休限定5000円以下ディナー。予算は抑えめ、でもみんなで美味しく楽しく食事がしたいカジュアルな会食シーンにぴったりのプランが満載です。",
      members: [ "user1","user22","user23","user14","user15","user16","user7", ],
    },
    "themePark": {
      title: "ワンダーランド年間パスポート",
      abstract: "大人気テーマパーク「ワンダーランド」の年間パスポートをプレゼント！カップルにもお子様連れにも最適です。",
      banner:"/assets/img/themePark-min.jpg",
      banners: {
        "banner0":{
          image:"/assets/img/themePark-min.jpg",
          featured: true,
        },
        "banner1":{
          image:"http://www.tokyodisneyresort.jp/blog/wp-content/uploads/2017/06/6927f120b1adc375b3b35cece7fac69a.jpg",
        },
        "banner2":{
          image:"http://articleimage.nicoblomaga.jp/image/23/2016/e/0/e00cb9cd79330091f66a052e7fc27939001fabd31472268680.jpg",
        },
      },
      detail: "全国のおすすめテーマパーク・レジャーランド354ヶ所をセレクト！人気の東京ディズニーランド(R)や東京ディズニーシー(R)などを口コミランキングでご紹介。全国のテーマパーク・レジャーランドスポットを探すならじゃらんnet。",
      members: [ "user1","user22","user23","user14","user15","user16","user7", ],
    },
    "wine": {
      title: "ワイン定期便",
      abstract: "毎月1本、ワインをお届けします。ワインに合うおつまみレシピもついた大変オトクなセット。もっとワインが好きになる！？",
      banner:"/assets/img/wine-min.jpg",
      banners: {
        "banner0":{
          image:"/assets/img/wine-min.jpg",
          featured: true,
        },
        "banner1":{
          image:"https://calori.jp/cms/wp-content/uploads/2017/07/wine.jpg",
        },
        "banner2":{
          image:"http://yamanashiwinetaxi.com/wp-content/uploads/2017/06/winetaxi2.jpg",
        },
      },
      detail: "ワインの好みはひとそれぞれ。高級なワインを選ぶことよりも、自分に合ったワインを飲んでほしいと思っています。カーヴ・ド・ヴァンは、ワイン一本一本の持つストーリーを最大限に引き出し、ワインを大切に思う皆さまにお届けいたします。",
      members: [ "user1","user22","user23","user14","user15","user16","user7", ],
    },
  },
  "companies": {
    "sales1": {
      "name": "営業一部",
      "color": orange500
    },
    "sales2": {
      "name": "営業二部",
      "color": amber500
    },
    "sales3": {
      "name": "営業三部",
      "color": yellow500
    },
    "develop": {
      "name": "開発本部",
      "color": indigo500
    },
    "corporate": {
      "name": "経営企画部",
      "color": blue500
    },
    "marketing": {
      "name": "マーケティング部",
      "color": cyan500
    },
    "finance": {
      "name": "財務部",
      "color": red500
    }
  }
};

export default data;