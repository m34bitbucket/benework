import { applyMiddleware, combineReducers, createStore } from 'redux';
import thunk from 'redux-thunk';

import appReducer from './reducers/App.js';
import detailReducer from './reducers/Detail.js';
import userReducer from './reducers/User.js';

const rootReducer = combineReducers({
  app: appReducer,
  detail: detailReducer,
  user: userReducer
});
const store = createStore(rootReducer, applyMiddleware(thunk));

export default store;
