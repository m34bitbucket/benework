import { grey50, grey900 } from 'material-ui/styles/colors';

export const app = {
  banner: {
    background: "url(/assets/img/banner.jpg) center / cover",
    height: 400
  },
  benefits: {
    padding: 100,

    title: {
      fontSize: "x-large",
      textAlign: "center"
    }
  },
  footer: {
    background: grey900,
    bottom: 0,
    color: grey50,
    padding: 30
  },
  onBanner: {
    padding: 200,
    paddingTop: 10
  },
  paper: {
    background: "rgba(0, 0, 0, 0.5)",
    padding: 30,
    paddingTop: 10
  },
  textField: {
    width: "100%"
  },
  grid: {
    tile: {
      height: 200,
      margin: 5
    }
  },
  title: {
    color: grey50,
    fontSize: "x-large",
    textAlign: "center"
  },
  mobile: {
    search: {
      paddingTop: "0px",
      paddingLeft: "20px",
      paddingRight: "20px",
      paddingBottom: "40px",
    }
  }
};

export const detail = {
  footer: {
    background: grey900,
    bottom: 0,
    color: grey50,
    padding: 30
  },
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
  },
  gridList: {
    width: 'auto',
  },
};

export const user = {
  footer: {
    background: grey900,
    bottom: 0,
    color: grey50,
    padding: 30
  },
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
  },
  gridList: {
    width: 'auto',
  },
  chip: {
    margin: 4,
  },
  wrapper: {
    display: 'flex',
    flexWrap: 'wrap',
  },
};
